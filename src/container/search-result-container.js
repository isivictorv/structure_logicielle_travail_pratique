// import React from 'react'
// import testSONGS from '../testSONGS.json' // testDATA
// fait par Dmytro

import React, { Component } from 'react'
// import testSONGS from '../testSONGS.json' // testDATA
import ResultElement from '../component/result-element'

class SearchResult extends Component {
    constructor (props) {
        super(props)

        this.state = {

            resultRecherche: props

        }
    }

    // componentDidMount () {
    //     const resultatMonRecherche = this.props.results
    //     console.log(resultatMonRecherche)
    //     if (resultatMonRecherche > 0) {
    //         this.setState({ resultRecherche: resultatMonRecherche })
    //     }
    // }

    render () {
        console.log('le resultat de state:', this.state.resultRecherche.props)
        // console.log(this.props.results, ' queremos saber si te vemos!')

        return (
            <div>
                {/* <p>{this.state.resultRecherche}</p> */}
                {/* {testSONGS.map((element, index) => <ResultElement element={element} key={index} />)} */}
                {this.state.resultRecherche !== '' ? this.state.resultRecherche.props.map((element, index) => <ResultElement src={element[2]} h3={element[0]} yearAlbum={element[5]} key={index} />) : 'no pasa nada en el serarch resault container'}
            </div>
        )
    }
}

export default SearchResult
