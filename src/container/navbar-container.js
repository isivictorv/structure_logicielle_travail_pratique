import React, { Component } from 'react'

import InputComponent from 'component/input-component'
import ButtonComponent from 'component/button-component'
import SelectComponent from 'component/select-component'
// import MusicData from '../music-data'
// import SearchResult from './search-result-container'
import MusicContainer from './music-container'
// import ResultElement from 'component/result-element'
// import MusicContainer from './music-container'

const OPTIONS = [{
    label: 'Default',
    value: '1'
}, {
    label: 'Acoustique',
    value: '2'
}, {
    label: 'Classique',
    value: '3'
}, {
    label: 'Country',
    value: '4'
}, {
    label: 'Metal',
    value: '5'
},
{
    label: 'Pop/Dance',
    value: '6'
},
{
    label: 'Rock',
    value: '7'
}]

class NavBarContainer extends Component {
    constructor (props) {
        super(props)

        this.state = {
            recherche: '',
            responseSelect: [],
            userToken: 'iaHjzOFrAscGALsGImsyRisiZKnqECcGSWtubDnK',
            resultRecherche: []

        }
        this.keyPress = this.keyPress.bind(this)
        this.handleInputOnChange = this.handleInputOnChange.bind(this)
        this.handleSave = this.handleSave.bind(this)
        this.rechercheSurDiscogs = this.rechercheSurDiscogs.bind(this)
        this.dataResult = this.dataResult.bind(this)
    }

    dataResult (param) {
        // console.log('dentro del callbackkkkkkk', param)
        this.setState({ resultRecherche: [param] })
        console.log('dentro del callbackkkkkkk', this.state.resultRecherche)
    }

    rechercheSurDiscogs (param) {
        const Discogs = require('disconnect').Client
        const discogs = new Discogs({ userToken: this.state.userToken }).database()
        discogs.search(this.state.recherche, { type: 'master', per_page: 50 }, (error, data) => {
            let testResult = 0
            if (data) {
                testResult = data.results.map((info, index) => [info.title, info.id, info.thumb, info.style, info.uri, info.year])
                // src={element.thumb} h3={element.title} styleAlbum={element.style} yearAlbum={element.year} hrefAlbum={element.uri} key={index}
            } else {
                throw new Error(alert('Oups somenthing happend please reload the page', error))
            }
            this.dataResult(testResult)
            console.log(testResult, 'hola')
        })
        // const myData = new MusicData(this.state.userToken)
        // myData.search(this.state.recherche, this.dataResult)
    }

    keyPress (event) {
        event.preventDefault()
        if (event.keyCode === 13) {
            console.log('value', event.target.value)
        }
    }

    handleInputOnChange (event) {
        console.log(event.target.value)
        this.setState({ recherche: event.target.value })
        // event.preventDefault()
    }

    handleSave (event) {
        console.log('dentro del handleSave')
        this.rechercheSurDiscogs()
    }

    render () {
        console.log(this.state)
        return (
            <div>
                <nav className='navbar navbar-dark bg-dark'>
                    <div className='nav-item dropdown'>
                        <form className='form-inline'>
                            <SelectComponent className='form-control mr-sm-2' id='music_id' name='music' options={OPTIONS} />
                            <ButtonComponent className='btn btn-light' name='Music' label='Music' />
                        </form>
                    </div>
                    <div className='form-inline'>
                        <InputComponent className='form-control mr-sm-2' type='text' id='search_id' name='search' placeholder='Search' onChange={this.handleInputOnChange} handleonkeydown={this.keyPress} />
                        <ButtonComponent className='btn btn-light' name='Music' label='Submit' onClick={this.handleSave} />

                    </div>

                </nav>

                {this.state.resultRecherche.length > 0 ? <MusicContainer results={this.state.resultRecherche} /> : 'no hay nada!'}

            </div>

        )
    }
}

export default NavBarContainer
