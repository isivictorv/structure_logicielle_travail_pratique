
import React, { Component } from 'react'
import DetailResultVideoComponent from '../component/detail-result-video-component'
import img from '../images/Apple-Music-icon-002.jpg'

class DetailContainer extends Component {
    constructor (props) {
        super(props)

        this.state = {
            props: props
        }
    }

    render () {
        return (
            <div>
                <div className='album' />
                <DetailResultVideoComponent
                    imgURL={img}
                    details=''
                    songList=''
                />

            </div>
        )
    }
}

export default DetailContainer
