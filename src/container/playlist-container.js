import React, { Component } from 'react'
import PlayListDB from '../testPlaylist.json'

class PlayListContainer extends Component {
    constructor (props) {
        super(props)

        this.state = {
            selected: props,
            backgroundColor: '#ffff00'
        }

        this.handleInputOnChange = this.handleInputOnChange.bind(this)
    }

    handleInputOnChange (event) {
        const selectedButton = event.target.name
        if (selectedButton !== undefined) {
            this.setState({
                selected: selectedButton === 'next' ? +1
                    : this.state.selected < 0 ? PlayListDB.length - 1 : 0
            })
        } else {
            this.setState({
                selected: event.target.id
            })
        }
    }

    render () {
        return (
            <div id='video-container'>
                <h3>{this.state.selected.props}</h3>
                {/* <div className='row'>
                    <video poster='' className='fullscreen-video' loop muted autoPlay controls>
                        <iframe className='embed-responsive-item'>
                            <source src={PlayListDB[this.state.selected].video} className='rounded float-left' type='video/mp4' id='video_id' name='video' autoPlay muted />
                        </iframe>
                    </video>
                    <div>
                        <ul> */}
                {/* {PlayListDB.map((element, index) => <li style={this.state.selected === element.id ? 'background-color : gray' : ''} key={element.id} id={index} onClick={this.handleInputOnChange}>{element.name}</li>)} */}
                {PlayListDB.map((element, index) => <li className='list-group-item' key={element.id} id={index} onClick={this.handleInputOnChange}>{element.name}</li>)}
                {/* </ul>
                    </div>
                </div>
                <h4>
                    {PlayListDB[this.state.selected].name}
                </h4>
                <div className='nav-item dropdown'>
                    <form className='form-inline'>
                        <button className='alert alert-dark' name='previous' onClick={this.handleInputOnChange}>Previous</button>
                        <button className='alert alert-dark' name='next' onClick={this.handleInputOnChange}>Next</button>
                    </form>
                </div> */}

            </div>
        )
    }
}

export default PlayListContainer
