import React, { Component } from 'react'
import SelectComponent from 'component/select-component'
import InputComponent from 'component/input-component'
import ButtonComponent from 'component/button-component'
import SearchResult from 'container/search-result-container'
import PlayListContainer from 'container/playlist-container'

class MusicContainer extends Component {
    constructor (props) {
        super(props)

        this.state = {
            research: '',
            playlist: '',
            options: '',
            recherche: '',
            responseSelect: '',
            selected: '',
            userToken: 'iaHjzOFrAscGALsGImsyRisiZKnqECcGSWtubDnK',
            resultRecherche: ''
        }

        this.handleSave = this.handleSave.bind(this)
        this.rechercheSurDiscogs = this.rechercheSurDiscogs.bind(this)
        this.dataResult = this.dataResult.bind(this)
        this.handleChage = this.handleChage.bind(this)
    }

    componentDidMount () {
        fetch('http://localhost:8081/getSelectoption/', {
            method: 'GET'
        }).then(response => response.json())
            .then((id) => this.setState({ options: id }))
    }

    dataResult (param) {
        this.setState({ resultRecherche: param })
        console.log('dentro del callbackkkkkkk', this.state.resultRecherche)
    }

    rechercheSurDiscogs () {
        const Discogs = require('disconnect').Client
        const discogs = new Discogs({ userToken: this.state.userToken }).database()

        discogs.search(this.state.recherche, { type: 'master', per_page: 50 }, (error, data) => {
            if (data) {
                this.setState({ resultRecherche: data.results.map((info, index) => [info.title, info.id, info.thumb, info.style, info.uri, info.year]) })
                // src={element.thumb} h3={element.title} styleAlbum={element.style} yearAlbum={element.year} hrefAlbum={element.uri} key={index}
            } else {
                throw new Error(alert('Oups somenthing happend please reload the page', error))
            }
        })
    }

    handleChage (event) {
        this.setState({ selected: event.taget.value })
    }

    handleSave (event) {
        this.setState({ recherche: 'madonna' })
        this.rechercheSurDiscogs()
    }

    render () {
        console.log('result recherche:', this.state.resultRecherche)
        return (
            <div>
                <nav className='navbar navbar-dark bg-dark'>
                    <div className='nav-item dropdown'>
                        <form className='form-inline'>
                            {this.state.options !== '' ? <SelectComponent className='form-control mr-sm-2' id='music_id' name='music' options={this.state.options} onChange={this.handleChage} /> : 'hi'}
                            <ButtonComponent className='btn btn-light' name='Music' label='Music' />
                        </form>
                    </div>
                    <div className='form-inline'>
                        <InputComponent className='form-control mr-sm-2' type='text' id='search_id' name='search' placeholder='Search' />
                        <ButtonComponent className='btn btn-light' name='Music' label='Submit' onClick={this.handleSave} />

                    </div>

                </nav>
                {this.state.resultRecherche !== '' ? <SearchResult props={this.state.resultRecherche} /> : <PlayListContainer props={this.state.selected} />}

            </div>
        )
    }
}

export default MusicContainer
