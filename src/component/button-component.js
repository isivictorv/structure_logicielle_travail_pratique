import React from 'react'

const ButtonComponent = ({ name, label, className, onClick }) => (
    <div>
        <button variant='primary' name={name} className={className} onClick={onClick}>{label} </button>{' '}
    </div>
)

export default ButtonComponent
