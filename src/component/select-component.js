import React from 'react'

function renderOption (option, index) {
    return <option value={option.id} key={index}>{option.title}</option>
    // return <option value={option} key={index}>{option}</option>
}

const SelectComponent = ({ text, id, name, value, options, className, onChange }) => (
    <div>
        <label htmlFor={id}>{text}</label>
        <select name={name} id={id} className={className} onChange={onChange}>
            {options.map((option, index) => renderOption(option, index, value))}
        </select>
    </div>
)

export default SelectComponent
