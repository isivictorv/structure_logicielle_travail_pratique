import React from 'react'

const DetailResultVideoComponent = ({ imgURL, props }) => (
    <div>
        <div className='left-column'>
            <img src={imgURL} />
            <ul className='list-group'>
                <li className='list-group-item'>
                    {props.title}
                </li>
                <li className='list-group-item'>
                    {props.style}
                </li>
                <li className='list-group-item'>
                    {props.year}
                </li>
            </ul>
        </div>
        <div className='list-group'>
            <button type='button' className='list-group-item list-group-item-action' onClick={() => insertSQL(props)}>Test</button>
        </div>
    </div>
)

function insertSQL (props) {
    fetch('http://localhost:8080/insert', { method: 'POST' })
        .then(response => response.json())
}
export default DetailResultVideoComponent
