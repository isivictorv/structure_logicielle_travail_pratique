import React from 'react'
// import DetailContainer from 'container/detail-container'

const ResultElement = ({ src, h3, yearAlbum }) => {
    return (
        <div className='product-item' style={{ margin: '10px' }}>
            <img src={src} className='rounded float-left' width='75' height='75' alt='Album-image' />
            <div>
                <h3>{h3}</h3>
                {/* <span>Style: {styleAlbum}</span><br /> */}
                <span>Year: {yearAlbum}</span><br />
                {/* <a href={hrefAlbum}>details</a> */}
            </div>
        </div>
    )
}

export default ResultElement
// import React from 'react'
// function generateurDeTaille (param) {
//     return (<div><h3>{param.element.name}</h3><span>Style: {param.element.style}</span><br /><span>Year: {param.element.year}</span><br /><a href={option.element.uri}>details</a></div>)
// }
// const ResultElement = (params) => {
//     return (
//         <div className='product-item'>
//             <lu>
//                <li key={index}><img src={param.element.thumb} className='rounded float-left' width='75' height='75' alt='Album-image' />{generateurDeTaille(param)}</li>)}
//             </lu>
//         </div>
//     )
// }

// export default ResultElement
