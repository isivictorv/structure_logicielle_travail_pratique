import React from 'react'

const InputComponent = ({ text, type, id, name, value, placeholder, className }) => (
    <div>
        <label htmlFor={id}>{text}</label>
        <input
            type={type}
            id={id}
            name={name}
            value={value}
            placeholder={placeholder}
            className={className}
            // onChange={onChange}
            // onKeyDown={onKeyDown}
        />
    </div>
)

export default InputComponent
