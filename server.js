'use strict'

// const inex = require('.')
const express = require('express')
const bodyParser = require('body-parser')

const app = express()
app.use(express.static('dist'))
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

app.use(bodyParser.json())

const dao = require('./src/dao')
const assert = require('assert').strict

const PORT = 8081
const HTTP_OK = 200
const CONTENT_TYPE_JSON = 'application/json'
// const CONTENT_TYPE_HTML = 'text/html'

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    next()
})

app.get('/getSelectoption', function (request, response) {
    dao.connect()
    let resultado = ''
    dao.query('select id,title from playlist', [], (result) => {
        assert.strictEqual(result.command, 'SELECT')
        resultado = result.rows
        dao.disconnect()
        response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_JSON })
        response.end(JSON.stringify(resultado))
    })
})

app.listen(PORT, function () {
    console.log('Server listening on: http://localhost:%s', PORT)
})
